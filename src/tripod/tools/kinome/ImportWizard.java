
package tripod.tools.kinome;

import java.io.File;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingworker.SwingWorker;
import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.jgoodies.looks.plastic.theme.*;

import tripod.ui.kinome.KinomePanel;


public class ImportWizard extends JFrame implements WizardListener {
    private static final Logger logger = 
	Logger.getLogger(ImportWizard.class.getName());

    private Wizard wizard;
    private KinomeViewer kv;
    private ImportModel model;

    public ImportWizard (KinomeViewer kv) {
        this.kv = kv;

        model = new ImportModel ();
        model.put("KinomePanel", kv.getKinomePanel());

	wizard = new Wizard (model);
	//wizard.setDefaultExitMode(Wizard.EXIT_ON_FINISH);
	wizard.addWizardListener(this);
	wizard.setOverviewVisible(true);

	//setDefaultCloseOperation (DO_NOTHING_ON_CLOSE);
	getContentPane().add(wizard);
	pack ();
        setSize (700, 500);
    }

    public void wizardCancelled (WizardEvent e) {
        logger.info("wizard cancel...");
        setVisible (false);
    }

    public void wizardClosed (WizardEvent e) {
        logger.info("wizard close...");
        setVisible (false);
        wizard.reset();
    }

    public void reset () {
        wizard.reset();
    }
}
