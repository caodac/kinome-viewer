
package tripod.tools.kinome;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import org.jdesktop.swingx.JXErrorPane;

import com.jgoodies.forms.layout.*;
import com.jgoodies.forms.builder.PanelBuilder;

import tripod.ui.kinome.*;


public class StepDataImport extends StepCommon 
    implements ActionListener, ListSelectionListener {

    private static final Logger logger = Logger.getLogger
	(StepDataImport.class.getName());

    public static final String NAME = "Data Import";
    public static final String DESC = 
	"Please setup your data file for import";

    static class DataValueModel extends AbstractTableModel 
        implements Comparator {

        String[] columns = new String[] {
            "Value",
            "Count"
        };

        Class[] classes = new Class[] {
            Object.class,
            Integer.class
        };

        Object[] rows;
        Map values;
        boolean isKinase;

        DataValueModel () {
        }

        void setValues (String name, Map values) {
            setValues (name, values, false);
        }

        void setValues (String name, Map values, boolean isKinase) {
            this.values = values;
            this.isKinase = isKinase;
            columns[0] = name;
            rows = values.keySet().toArray(new Object[0]);
            Arrays.sort(rows, this);
            fireTableStructureChanged ();
        }

        void clear () {
            values = null;
            rows = null;
            columns[0] = "Value";
            fireTableStructureChanged ();
        }
            
        public int compare (Object key1, Object key2) {
            Integer c1 = (Integer)values.get(key1);
            Integer c2 = (Integer)values.get(key2);
            if (c1 == null && c2 == null) return 0;
            if (c1 == null && c2 != null) return -1;
            if (c1 != null && c2 == null) return 1;
            return c2 - c1;
        }

        public int getColumnCount () { 
            return isKinase ? (columns.length+1) : columns.length; 
        }
        public int getRowCount () { 
            return values != null ? values.size() : 0; 
        }
        public String getColumnName (int col) { 
            return col < columns.length ? columns[col] : "Kinase Gene?";
        }
        public Class getColumnClass (int col) { 
            return col < classes.length ? classes[col] : Boolean.class; 
        }
        public Object getValueAt (int row, int col) {
            if (col == 0) {
                return rows[row];
            }
            if (col < columns.length)
                return values.get(rows[row]);

            // is kinase?
            return KinomeDataset.isValidKinase((String)rows[row]);
        }
    }


    JCheckBox headerCb;
    JTable table, viewtab;
    String delimiter = "CSV";

    public StepDataImport () {
	super (NAME, DESC);
    }

    @Override
    protected void initUI () {
        JPanel pane = new JPanel (new BorderLayout (0, 2));
        pane.add(createOptionPane (), BorderLayout.NORTH);

        JSplitPane split = new JSplitPane (JSplitPane.VERTICAL_SPLIT);
        split.setTopComponent(createTablePane ());
        split.setBottomComponent(createViewPane ());
        split.setDividerSize(5);
        split.setResizeWeight(.5);
        pane.add(split);

        setLayout (new BorderLayout ());
        add (pane);
    }

    Component createOptionPane () {
        JPanel pane = new JPanel (new BorderLayout ());
        pane.setBorder(BorderFactory.createCompoundBorder
                       (BorderFactory.createTitledBorder(""),
                        BorderFactory.createEmptyBorder(1,1,1,1)));
        Box box = Box.createHorizontalBox();
        box.add(headerCb = new JCheckBox ("Header"));
        headerCb.setSelected(true);
        headerCb.setToolTipText("Is the first line a header?");
        box.add(Box.createHorizontalStrut(10));

        Object[][] delimiters = new Object[][] {
            {new JRadioButton ("CSV"), "Comma delimited file"},
            {new JRadioButton ("Tab"), "Tab delimited file"},
            {new JRadioButton ("Space"), "Space delimited file"}
        };

        ButtonGroup bg = new ButtonGroup ();
        for (int i = 0; i < delimiters.length; ++i) {
            box.add(Box.createHorizontalStrut(2));
            AbstractButton ab = (AbstractButton)delimiters[i][0];
            ab.addActionListener(this);
            String text = (String)delimiters[i][1];
            ab.setToolTipText(text);
            box.add(ab);
            bg.add(ab);
            ab.setSelected(i == 0);
        }
        pane.add(box);

        JButton btn = new JButton ("Load");
        btn.setToolTipText("Load data file");
        btn.addActionListener(this);
        pane.add(btn, BorderLayout.EAST);

        return pane;
    }

    Component createTablePane () {
        JPanel pane = new JPanel (new BorderLayout ());
        pane.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        pane.add(new JScrollPane (table = new JTable
                                  (new DataImportModel ())));
        table.getSelectionModel()
            .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.getSelectionModel().addListSelectionListener(this);

        //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setDefaultEditor
            (DataLabel.class, new DefaultCellEditor 
             (new JComboBox (EnumSet.allOf(DataLabel.class)
                             .toArray(new DataLabel[0]))));
        table.setDefaultEditor
            (DataType.class, new DefaultCellEditor 
             (new JComboBox (EnumSet.allOf(DataType.class)
                             .toArray(new DataType[0]))));

        return pane;
    }

    Component createViewPane () {
        JPanel pane = new JPanel (new BorderLayout ());
        pane.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        pane.add(new JScrollPane (viewtab = new JTable
                                  (new DataValueModel ())));
        return pane;
    }

    public void valueChanged (ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) return;
        int row = table.getSelectedRow();
        if (row >= 0) {
            row = table.convertRowIndexToModel(row);
            DataImportModel model = (DataImportModel)table.getModel();
            DataLabel label = (DataLabel)model.getValueAt(row, 2);
            Map values = model.getValues(row);
            ((DataValueModel)viewtab.getModel()).setValues
                ((String)model.getValueAt(row, 0), values, 
                 label != null && label == DataLabel.Kinase);
        }
    }

    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();
        if ("load".equalsIgnoreCase(cmd)) {
            doLoad ();
        }
        else if (e.getSource() instanceof JRadioButton) {
            delimiter = cmd;
        }
    }

    protected void doLoad () {
        JFileChooser chooser = getFileChooser ();
        chooser.setDialogTitle("Please select input data file");
        int op = chooser.showOpenDialog(this);
        if (JFileChooser.APPROVE_OPTION == op) {
            try {
                File file = chooser.getSelectedFile();
                loadData (file);
                ((DataValueModel)viewtab.getModel()).clear();
            }
            catch (Exception ex) {
                JXErrorPane.showDialog(ex);
            }
        }
    }

    protected void loadData (File file) throws IOException {
        DataImportModel model = (DataImportModel)table.getModel();
        String sep = "";
        if ("csv".equalsIgnoreCase(delimiter)) 
            sep = ",";
        else if ("tab".equalsIgnoreCase(delimiter))
            sep = "[\\t]";
        else if ("space".equalsIgnoreCase(delimiter))
            sep = "[\\s]";
        else
            throw new IllegalStateException
                ("Unknown delimiter option: "+delimiter);

        model.load(file, headerCb.isSelected(), sep);
    }

    @Override
    public void applyState () throws InvalidStateException {
        // make sure we have at least Kinase and Activity labels 
        DataImportModel model = (DataImportModel)table.getModel();

        EnumSet<DataLabel> labels = model.getLabels();
        if (!labels.contains(DataLabel.Kinase))
            throw new InvalidStateException 
                ("At least one (and only one) column must be labeled Kinase!");
        if (!labels.contains(DataLabel.Activity))
            throw new InvalidStateException 
                ("At least one (and only one) column must "
                 +"be labeled Activity!");

        getModel().put("DataImportModel", model);
    }

    public static void main (String[] argv) throws Exception {
        SwingUtilities.invokeLater(new Runnable () {
                public void run () {
                    JFrame f = new JFrame ();
                    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    StepDataImport ss = new StepDataImport ();
                    f.getContentPane().add(ss);
                    f.setSize(new Dimension (400, 400));
                    f.pack();
                    f.setVisible(true);
                }
            });
    }
}
