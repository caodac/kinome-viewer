package tripod.tools.kinome;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;

import java.awt.Color;
import tripod.ui.kinome.KinomeDataset;
import tripod.ui.kinome.DefaultKinomeRendererFactory;
import tripod.ui.kinome.KinomeItemRenderer;

public class RenderedKinomeDataset extends DefaultKinomeRendererFactory {
    protected Map<Number, KinomeItemRenderer> renderers = 
        new TreeMap<Number, KinomeItemRenderer>();
    protected KinomeDataset dataset;

    public RenderedKinomeDataset () {
    }
    public RenderedKinomeDataset (Color color) {
        this (null, color);
    }
    public RenderedKinomeDataset (KinomeDataset dataset, Color color) {
        super (color);
        this.dataset = dataset;
    }

    public KinomeDataset getDataset () { return dataset; }
    public void setDataset (KinomeDataset dataset) {
        this.dataset = dataset;
    }

    public void setTiny (Number value) {
        setValue (value, tiny);
    }
    public Number getTiny () { return getValue (tiny); }

    public void setSmall (Number value) {
        setValue (value, small);
    }
    public Number getSmall () { return getValue (small); }

    public void setMedium (Number value) {
        setValue (value, medium);
    }
    public Number getMedium () { return getValue (medium);  }

    public void setLarge (Number value) {
        setValue (value, large);
    }
    public Number getLarge () { return getValue (large);  }
        

    protected Number getValue (KinomeItemRenderer renderer) {
        for (Map.Entry<Number, KinomeItemRenderer> me 
                 : renderers.entrySet()) {
            if (me.getValue() == renderer) return me.getKey();
        }
        return null;
    }
    protected void setValue (Number value, KinomeItemRenderer renderer) {
        ArrayList<Number> remove = new ArrayList<Number>();
        for (Map.Entry<Number, KinomeItemRenderer> me 
                 : renderers.entrySet()) {
            if (me.getValue() == renderer) {
                remove.add(me.getKey());
            }
        }

        for (Number n : remove) {
            renderers.remove(n);
        }

        renderers.put(value, renderer);
    }

    @Override
    public KinomeItemRenderer getRenderer (String kinase, Number value) {
        double x = value.doubleValue();
        for (Map.Entry<Number, KinomeItemRenderer> me 
                 : renderers.entrySet()) {
            if (x <= me.getKey().doubleValue())
                return me.getValue();
        }
        return null;
    }
}
