
package tripod.tools.kinome;

import java.util.*;
import java.io.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.awt.Color;

import java.awt.Component;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdesktop.swingx.JXErrorPane;
import com.jgoodies.looks.plastic.PlasticFileChooserUI;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import tripod.ui.kinome.*;


public class StepCommon extends PanelWizardStep {
    private static final Logger logger = Logger.getLogger
	(StepCommon.class.getName());

    static final ImageIcon LOGO = new ImageIcon 
	(StepCommon.class.getResource("resources/gleevec.png"));

    enum DataLabel {
        None,
            Dataset,
            Kinase,
            Activity
            }

    // ordered from most generic to specific
    enum DataType {
        String (String.class),
            Float (Double.class),
            Integer (Integer.class);

        final Class cls;
        DataType (Class cls) {
            this.cls = cls;
        }

        Class toClass () { return cls; }
    }

    static class DataImportModel extends AbstractTableModel {
        String[] columns = new String[] {
            "Column",
            "Cardinality",
            "Label",
            "Type"
        };

        Class[] classes = new Class[] {
            String.class,
            Integer.class,
            DataLabel.class,
            DataType.class
        };

        ArrayList rows = new ArrayList ();
        Map[] values; // unique values for each column
        // all data for each column
        ArrayList<String[]> data = new ArrayList<String[]>();

        protected DataImportModel () {
        }

        public int getRowCount () { return rows.size(); }
        public int getColumnCount () { return columns.length; }
        public String getColumnName (int col) { return columns[col]; }
        public Class getColumnClass (int col) { return classes[col]; }
        public Object getValueAt (int row, int col) {
            Object[] r = (Object[])rows.get(row);
            if (col == 2 && ((DataLabel)r[col] == DataLabel.None))
                return null;
            return r[col];
        }
        public void setValueAt (Object value, int row, int col) {
            Object[] r = (Object[])rows.get(row);
            r[col] = value;
        }
        public boolean isCellEditable (int row, int col) {
            return col > 1;
        }

        public void load (File file, boolean hasHeader, String delimiter) 
            throws IOException {
            logger.info("## file="+file+"; header="+hasHeader
                        +"; delimiter="+delimiter);

            BufferedReader br = new BufferedReader (new FileReader (file));

            String[] header = null;
            DataType[] types = null;
            values = null;

            rows.clear();
            int lines = 1;
            for (String line; (line = br.readLine()) != null; ++lines) {
                String[] row = line.split
                    (delimiter, header != null ? header.length : 0);
                if (header == null) {
                    if (hasHeader) {
                        header = row;
                    }
                    else {
                        header = new String[row.length];
                        for (int i = 0; i < header.length; ++i) 
                            header[i] = "Column "+(i+1);
                        data.add(row);
                    }
                    values = new Map[row.length];
                    types = new DataType[row.length];

                    for (int i = 0; i < row.length; ++i) {
                        values[i] = new HashMap ();
                        if (header != row) {
                            types[i] = getType (row[i]);
                            values[i].put(row[i], 1);
                        }
                        rows.add(new Object[]{row[i], 0, 
                                              DataLabel.None, types[i]});
                    }
                }
                else {
                    if (row.length != header.length) {
                        logger.warning("Line "+lines
                                       +": mismatch column count; expecting "
                                       +header.length+" but got "+row.length);
                    }
                    else {
                        for (int i = 0; i < row.length; ++i) {
                            if (row[i].trim().length() > 0) {
                                Integer c = (Integer)values[i].get(row[i]);
                                values[i].put(row[i], c!=null ? (c+1):1);

                                DataType type = getType (row[i]);
                                //logger.info(i+": "+row[i]+" => "+type);
                                if (types[i] == null
                                    || types[i].ordinal() > type.ordinal())
                                    types[i] = type;
                            }
                        }
                        data.add(row);
                    }
                }
            }
            br.close();

            if (values != null) {
                // initial assignment of label
                EnumSet<DataLabel> labels = EnumSet.noneOf(DataLabel.class);
                Object[] intRow = null;
                for (int i = 0; i < values.length; ++i) {
                    Object[] r = (Object[])rows.get(i);
                    r[1] = values[i].size();
                    r[r.length-1] = types[i];
                    switch (types[i]) {
                    case String:
                        if (!labels.contains(DataLabel.Dataset)) {
                            labels.add(DataLabel.Dataset);
                            r[2] = DataLabel.Dataset;
                        }
                        else if (!labels.contains(DataLabel.Kinase)) {
                            labels.add(DataLabel.Kinase);
                            r[2] = DataLabel.Kinase;
                        }
                        break;
                    case Float:
                        if (!labels.contains(DataLabel.Activity)) {
                            labels.add(DataLabel.Activity);
                            r[2] = DataLabel.Activity;
                        }
                        break;

                    case Integer:
                        if (intRow == null) {
                            intRow = r;
                        }
                        break;
                    }
                }

                if (!labels.contains(DataLabel.Activity)) {
                    if (intRow != null) {
                        intRow[2] = DataLabel.Activity;
                    }
                    else {
                        // look like data contains all strings... should
                        //  we cry foul here?
                    }
                }
            }

            fireTableStructureChanged ();
        }

        DataType getType (String value) {
            try {
                // assuming double format IEEE 754 
                long lx = Double.doubleToLongBits(Double.parseDouble(value));
                long m = lx & 0x000fffffffffffffl;
                int e = (int)((lx >> 52) & 0x7ffl);
                long d = 1l<<(1075 - e);
                return m % d == 0 ? DataType.Integer : DataType.Float;
            }
            catch (NumberFormatException ex) {
                return DataType.String;
            }
        }

        public EnumSet<DataLabel> getLabels () {
            EnumSet<DataLabel> labels = EnumSet.noneOf(DataLabel.class);
            for (Object obj : rows) {
                Object[] r = (Object[])obj;
                if (r[2] != null) {
                    labels.add((DataLabel)r[2]);
                }
            }
            return labels;
        }

        public Map getValues (int row) {
            return values != null ? values[row] : null;
        }

        public Map getValues (DataLabel label) {
            ArrayList data = new ArrayList ();
            for (int r = 0; r < rows.size(); ++r) {
                Object[] row = (Object[])rows.get(r);
                if (row[2] != null && row[2].equals(label)) {
                    return getValues (r);
                }
            }
            return null;
        }

        public KinomeDataset getDataset (String name) {
            int dsetcol = getColumn (DataLabel.Dataset);
            // these two columns must be defined!
            int actcol = getColumn (DataLabel.Activity);
            int kincol = getColumn (DataLabel.Kinase);

            if (actcol < 0 || kincol < 0) {
                throw new IllegalStateException 
                    ("Data table doesn't contain either "
                     +"Activity or Kinase label!");
            }

            KinomeDataset kd = null;
            if (dsetcol < 0) { // => name is also null
                if (name != null || name.equals("")) {
                    logger.warning("No Dataset label yet requesting "
                                   +"dataset \""+name+"\"; ignoring...");
                }

                kd = new KinomeDataset ();
                int bogus = 0;
                for (Iterator<String[]> iter = data.iterator(); 
                     iter.hasNext(); ) {
                    String[] row = iter.next();
                    try {
                        kd.setValue(row[kincol], 
                                    Double.parseDouble(row[actcol]));
                    }
                    catch (NumberFormatException ex) {
                        ++bogus;
                    }
                }

                if (bogus > 0) {
                    logger.warning(bogus+" bogus activities ignored!");
                }
            }
            else {
                kd = new KinomeDataset (name);

                int bogus = 0;
                for (Iterator<String[]> iter = data.iterator(); 
                     iter.hasNext(); ) {
                    String[] row = iter.next();
                    if (row[dsetcol].equals(name)) {
                        try {
                            kd.setValue(row[kincol], 
                                        Double.parseDouble(row[actcol]));
                        }
                        catch (NumberFormatException ex) {
                            ++bogus;
                        }
                    }
                }

                if (bogus > 0) {
                    logger.warning(bogus+" bogus activities ignored!");
                }
            }

            return kd;
        }

        public Class getClass (DataLabel label) {
            for (int c = 0; c < rows.size(); ++c) {
                Object[] row = (Object[])rows.get(c);
                if (row[2] != null && row[2].equals(label)) {
                    DataType type = (DataType)row[3];
                    return type.toClass();
                }
            }
            return null;
        }

        public int getColumn (DataLabel label) {
            for (int c = 0; c < rows.size(); ++c) {
                Object[] row = (Object[])rows.get(c);
                if (row[2] != null && row[2].equals(label)) {
                    return c;
                }
            }
            return -1;
        }
    }

    protected JFileChooser fileChooser;
    protected ImportModel model;
    protected JColorChooser colorChooser;

    protected StepCommon (String name, String desc) {
	this (name, desc, LOGO);
    }

    protected StepCommon (String name, String desc, Icon icon) {
	super (name, desc, icon);
	initUI ();

	setComplete (true);
    }

    // Override by subclasses to provide meaningful UI
    protected void initUI () {
    }

    protected JFileChooser getFileChooser () {
	if (fileChooser == null) {
	    fileChooser = new JFileChooser 
		("."/*System.getProperty("user.home")*/);
	    new PlasticFileChooserUI (fileChooser);
	}

	return fileChooser;
    }


    protected JColorChooser getColorChooser () {
        if (colorChooser == null) {
            colorChooser = new JColorChooser ();
        }
        return colorChooser;
    }

    @Override
    public void init (WizardModel model) {
	this.model = (ImportModel)model;
    }

    protected ImportModel getModel () { return model; }


    static void addSeparator (JPanel pane, String title) {
	pane.add(new JLabel (title), 
		 "gapbottom 1, span, split 2, aligny center");
	pane.add(new JSeparator (), "gapleft rel,growx");	
    }
}
