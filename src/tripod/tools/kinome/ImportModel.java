
package tripod.tools.kinome;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

public class ImportModel extends StaticModel {
    private static final Logger logger = 
	Logger.getLogger(ImportModel.class.getName());

    protected Hashtable props = new Hashtable ();
    
    public ImportModel () {
        add (new StepDataImport ());
        add (new StepConfig ());
    }

    public void put (String prop, Object value) {
        props.put(prop, value);
    }
    public Object get (String prop) { return props.get(prop); }
}
