package tripod.ui.base;

// provide tooltip for grid cell
public interface GridCellTipAnnotator {
    String getGridCellTipAnnotation (Grid g, Object value, int cell);
}
