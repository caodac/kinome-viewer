
package tripod.ui.kinome;

import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.Component;
import java.awt.Color;
import java.awt.Rectangle;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import tripod.ui.base.GridCellRenderer;
import tripod.ui.base.Grid;

public class KinomeCellRenderer 
    extends KinomePanel implements TableCellRenderer, GridCellRenderer {

    private static final Logger logger = 
	Logger.getLogger(KinomeCellRenderer.class.getName());

    static final KinomeRendererFactory[] factories = {
	new DefaultKinomeRendererFactory (Color.red),
	//new DefaultKinomeRendererFactory (Color.blue),
	new DefaultKinomeRendererFactory (new Color (204,0,204)),
	new DefaultKinomeRendererFactory (new Color (153,153,0)),
	new DefaultKinomeRendererFactory (new Color (0,204,204)),
	new DefaultKinomeRendererFactory (new Color (102,0,102)),
	new DefaultKinomeRendererFactory (new Color (51,51,51))
    };

    static final Component BLANK = new KinomePanel (false);


    public KinomeCellRenderer () {
	setBackground (Color.white);
	setPaintBorder (false);
	//setBorder (javax.swing.BorderFactory.createEmptyBorder(2,2,2,2));
    }

    public Component getTableCellRendererComponent
	(JTable table, Object value, boolean isSelected, 
	 boolean hasFocus, int row, int column) {
	//logger.info("row="+row+" col="+column+": "+value);

	if (value != null) {
	    setValue (value);
	    return this;
	}

	return BLANK;
    }

    public Component getGridCellRendererComponent
	(Grid g, Object value, boolean selected, int cell) {
	if (value != null) {
	    setValue (value);
	    return this;
	}

	return BLANK;
    }

    public void setValue (Object value) {
	clear ();
	if (value == null) {
	    return;
	}

	if (value instanceof KinomeDataset[]) {
	    KinomeDataset[] kds = (KinomeDataset[])value;
	    if (kds.length == 1) {
		add (kds[0], factories[0]);
	    }
	    else {
		for (int i = 0; i < kds.length; ++i) {
		    if (kds[i] != null) {
			add (kds[i], factories[1+(i%(factories.length-1))]);
		    }
		}
	    }
	}
	else if (value instanceof KinomeDataset) {
	    add ((KinomeDataset)value, factories[0]);
	}
    }
}
