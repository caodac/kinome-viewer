Kinome Tools and Components
===========================

This repository contains source code to the kinome Java component and 
related tools. A quick on-line demo of one of the tools is available 
[here](http://tripod.nih.gov/?p=226). To run this tool from source, 
you'll need Apache Ant.

```
ant kinome
```

A more customizable viewer is also available. To build the viewer, use the 
ant task `viewer`, i.e.,

```
ant viewer
```

This tools accepts CSV format of the form:

```
Compound,Gene,Activity
Compound 1,CLK1,10
Compound 1,CLK2,10
Compound 1,CLK4,10
Compound 1,CSNK1E,30
Compound 1,DYRK1A,10
Compound 1,MAP3K1,30
Compound 1,PRKCE,10
Compound 2,aak1,30
Compound 2,bike,10
Compound 2,clk1,30
Compound 2,clk2,10
Compound 2,clk4,30
Compound 2,dyrk1a,10
Compound 2,gak,30
Compound 2,lats1,30
Compound 2,pim1,30
Compound 2,pip5k1a,30
Compound 2,pip4k2b,30
Compound 2,riok3,30
Compound 2,srpk3,30
```

Here are a few screenshots of the tool:

![wizard1](http://tripod.nih.gov/ws/kinome/kinome_wizard1.png)
![wizard2](http://tripod.nih.gov/ws/kinome/kinome_wizard2.png)
![viewer](http://tripod.nih.gov/ws/kinome/kinome-viewer.png)

Please forward questions and/or comments about the code and/or tools
to <nguyenda@mail.nih.gov>.


Copyright
=========

The original kinome dendrogram is available [here](http://www.sciencemag.org/content/suppl/2002/12/13/298.5600.1912.DC2/KinomePoster.pdf).


Disclaimer
==========

                         PUBLIC DOMAIN NOTICE
                     NIH Chemical Genomics Center
         National Center for Advancing Translational Sciences

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software is freely available to the
public for use. The NIH Chemical Genomics Center (NCGC) and the
U.S. Government have not placed any restriction on its use or
reproduction.

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.
